// Proyecto2.cpp : Defines the entry point for the application.
//

#include "framework.h"
#include "Proyecto2.h"
#include <CommCtrl.h>
#include <vector>
#include <shlwapi.h>
#include <memory.h>

using namespace std;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND hWnd; //handle de main window
HWND tree_hWnd; //handle de tree window
HWND list_hWnd; //handle de list
HWND backButton_hWnd;
HWND forwardButton_hWnd;
HWND path_hWnd;
char currPath[2048] = "C:\\";
char itemSelectedDropDownName[100];
char itemSelectedToOpen[100];


// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK NewFile(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK NewDir(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK Move(HWND, UINT, WPARAM, LPARAM);
int createFile(HWND handler, char fileName[100], char contenido[4096], UINT size);
int createDir(HWND handler, char dirName[100]);
void goBack();

void deleteDirNotEmpty(char* dir);

int getDirectoryContents(char* dir, HTREEITEM parent);

bool isDir(char* name);

//HTREEITEM agregarNodoATree(char* name, HTREEITEM parent, bool isDir);

char dir2[5] = "C:\\";
void AddItemToList(char* item);
void refreshListView(char* path);

void initBotonesYPath();
void initTreeView();

INT_PTR CALLBACK VisualWindow(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	// register TreeView from comctl32.dll before creating windows
	/*INITCOMMONCONTROLSEX commonCtrls;
	commonCtrls.dwSize = sizeof(commonCtrls);
	commonCtrls.dwICC = ICC_TREEVIEW_CLASSES;   // TreeView class name
	InitCommonControlsEx(&commonCtrls);

	INITCOMMONCONTROLSEX icex;           // Structure for control initialization.
	icex.dwICC = ICC_LISTVIEW_CLASSES;
	InitCommonControlsEx(&icex); */



	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_PROYECTO2, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROYECTO2));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROYECTO2));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_PROYECTO2);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindowW(szWindowClass, L"File Explorer", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, 800, 600, nullptr, nullptr, hInstance, nullptr);

	initBotonesYPath();
	initTreeView();

	if (!hWnd) {
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

void initTreeView() {
	/****** TREEVIEW ******/
	InitCommonControls();
	tree_hWnd = CreateWindowEx(0, WC_TREEVIEW, L"TREE VIEW", WS_CHILD | WS_VISIBLE | WS_BORDER | WS_SIZEBOX | WS_VSCROLL | WS_TABSTOP | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | TVS_SHOWSELALWAYS,
		0, 40, 200, 502, hWnd, (HMENU)IDM_TREEVIEW, hInst, nullptr);

	char dir[100] = "C:\\Users\\mstig\\Documents\\Siso2\\Proyecto2\\";


	//HTREEITEM par = AddItemToTree(hWnd, dir, TVI_ROOT,true);
	//HTREEITEM par = agregarNodoATree(dir, TVI_ROOT, true);
	getDirectoryContents(dir2, (HTREEITEM)dir2);
}
void initBotonesYPath() {



	/****** FORWARD BACK BUTTON ******/


	backButton_hWnd = CreateWindowEx(
		0,  // Predefined class; Unicode assumed 
		L"BUTTON",
		L"",// Button text 
		WS_VISIBLE | WS_CHILD | BS_PUSHLIKE | BS_ICON,  // Styles 
		45,         // x position 
		6,         // y position 
		45,        // Button width
		30,        // Button height
		hWnd,     // Parent window
		NULL,       // No menu.
		hInst,
		NULL);      // Pointer not needed.
	HICON hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_BACK));
	SendMessage(backButton_hWnd, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)hIcon);


	DestroyIcon(hIcon);

	path_hWnd = CreateWindowExA(
		0,  // Predefined class; Unicode assumed 
		"EDIT",
		currPath,// Button text 
		WS_VISIBLE | WS_CHILD | WS_BORDER,  // Styles 
		210,         // x position 
		10,         // y position 
		550,        // Button width
		25,        // Button height
		hWnd,     // Parent window
		NULL,       // No menu.
		hInst,
		NULL);      // Pointer not needed.

}

void goBack() {
	char temp[2048];
	GetWindowTextA(path_hWnd, temp, 2048);
	int s = strlen(temp);
	//	OutputDebugStringA(to_string(s).c_str());
		//OutputDebugStringA("\n");
	temp[s - 1] = ' ';
	//OutputDebugStringA(temp);
	//OutputDebugStringA("\n");
	int a;
	for (a = strlen(temp) - 1; a > 0; a--) {

		if (temp[a] == '\\') {
			//OutputDebugStringA("here");
			//OutputDebugStringA("\n");
			break;
		}
	}

	memset(currPath, 0, 2048);
	//OutputDebugStringA(to_string(a).c_str());
	//OutputDebugStringA("\n");
	for (int b = 0; b <= a; b++) {

		currPath[b] = temp[b];
	}

	OutputDebugStringA(currPath);
	//strcat(currPath, itemSelectedToOpen);
	//strcat(currPath, "\\");
	//OutputDebugStringA(currPath);
	//OutputDebugStringA("\n");
	SetWindowTextA(path_hWnd, currPath);

}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{

	case WM_COMMAND:
	{

		//DialogBox(hInst, MAKEINTRESOURCE(IDD_MAINWINDOW), hWnd, VisualWindow);
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case ID_NEW_ARCHIVO:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_NEWFILE), hWnd, NewFile);
			break;
		case ID_NEW_CARPETA:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_NEWDIR), hWnd, NewDir);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case BN_CLICKED:
		{
			if ((HWND)lParam == backButton_hWnd) {
				goBack();
				refreshListView(currPath);
			}
		}
		break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_NOTIFY:

		switch (((LPNMHDR)lParam)->code)
		{
		case NM_DBLCLK:
		{
			int iSlected = SendMessage(list_hWnd, LVM_GETNEXTITEM, -1, LVNI_FOCUSED);
			LVITEM LvItem;
			char Text[256];

			LvItem.mask = LVIF_TEXT;
			LvItem.iSubItem = 0;
			LvItem.pszText = (LPWSTR)Text;
			LvItem.cchTextMax = 256;
			LvItem.iItem = iSlected;

			SendMessage(list_hWnd, LVM_GETITEMTEXT,
				iSlected, (LPARAM)& LvItem);
			size_t size = wcstombs(NULL, (wchar_t*)LvItem.pszText, 0);
			//char* charStr = new char[size + 1];
			wcstombs(itemSelectedToOpen, (wchar_t*)LvItem.pszText, size + 1);

			//OutputDebugStringA(itemSelectedToOpen);
			if (isDir(itemSelectedToOpen)) {
				GetWindowTextA(path_hWnd, currPath, 2048);
				//OutputDebugStringA(currPath);
				strcat(currPath, itemSelectedToOpen);
				strcat(currPath, "\\");
				//OutputDebugStringA(currPath);
				//OutputDebugStringA("\n");
				SetWindowTextA(path_hWnd, currPath);
				refreshListView(currPath);
			}
			else { //abrirlo con el sistema operativo

			}

		}
		break;
		case NM_RCLICK:
		{
			int iSlected = SendMessage(list_hWnd, LVM_GETNEXTITEM, -1, LVNI_FOCUSED);
			LVITEM LvItem;
			char Text[256];

			LvItem.mask = LVIF_TEXT;
			LvItem.iSubItem = 0;
			LvItem.pszText = (LPWSTR)Text;
			LvItem.cchTextMax = 256;
			LvItem.iItem = iSlected;

			SendMessage(list_hWnd, LVM_GETITEMTEXT,
				iSlected, (LPARAM)& LvItem);
			size_t size = wcstombs(NULL, (wchar_t*)LvItem.pszText, 0);
			//char* charStr = new char[size + 1];
			wcstombs(itemSelectedDropDownName, (wchar_t*)LvItem.pszText, size + 1);

			//OutputDebugStringA(itemSelectedDropDownName);
			//OutputDebugStringA("\n");

			POINT cursor; // Getting the cursor position
			GetCursorPos(&cursor);

			int n = TrackPopupMenu((HMENU)GetSubMenu(LoadMenu(hInst, MAKEINTRESOURCE(IDR_MENU2)), 0), TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD, cursor.x, cursor.y, 0, list_hWnd, NULL);
			switch (n)
			{
			case ID_COPIAR_BORRAR: // MOVER
			{
				//OutputDebugStringA("move");
				DialogBox(hInst, MAKEINTRESOURCE(IDD_MOVE), hWnd, Move);
				break;
			}
			break;
			case ID_COPIAR_MOVER: //COPIAR
			{
				//OutputDebugStringA("copy");
				char nuevo[100];
				char actual[100];
				strcpy(nuevo, currPath);
				strcpy(actual, currPath);
				strcat(nuevo, itemSelectedDropDownName);
				strcat(nuevo, "_1");
				strcat(actual, itemSelectedDropDownName);
				// OutputDebugStringA(actual);
				// OutputDebugStringA("\n");
				// OutputDebugStringA(nuevo);
				// OutputDebugStringA("\n");
				BOOL m = CopyFileA(actual, nuevo, true);
				if (m == false) {
					wchar_t err[256];
					memset(err, 0, 256);
					FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
						MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
					int msgboxID = MessageBoxW(NULL,
						err,
						(LPCWSTR)L"ERROR",
						MB_OK);
				}

				// AddItemToList(currPath); BUSCAR LA FORMA DE HACER UN REFRESH DE LOS ITEMS
			}
			break;
			case ID_COPIAR_BORRAR32778: // BORRAR
			{
				if (isDir(itemSelectedDropDownName)) {
					// OutputDebugStringA("dir");
					// OutputDebugStringA("\n");
					char aBorrar[2048];
					strcpy(aBorrar, currPath);
					strcat(aBorrar, itemSelectedDropDownName);
					OutputDebugStringA("A borrar: ");
					OutputDebugStringA(aBorrar);
					OutputDebugStringA("\n");
					if (PathIsDirectoryEmptyA(aBorrar)) {
						OutputDebugStringA("entro");
						// OutputDebugStringA("\n");
						RemoveDirectoryA(aBorrar);
						refreshListView(currPath);
					}
					else {
						//borrar todo lo de adentro
						char dirNom[2048];
						strcpy(dirNom, currPath);
						/* char temp[2048];
						 strcpy(temp, currPath);
						 int a;
						 for (a = strlen(temp); a > 0; a--) {
							 if (temp[a] == '\\') {
								 break;
							 }
						 }
						 memset(dirNom, 0, 2048);
						 for (int b = 0; b <= a; b++) {
							 dirNom[b] = temp[b];
						 } */
						strcat(dirNom, itemSelectedDropDownName);
						deleteDirNotEmpty(dirNom);
					}
				}
				else {
					// OutputDebugStringA("delete");
					// OutputDebugStringA("\n");
					char aBorrar[2048];
					strcpy(aBorrar, currPath);
					strcat(aBorrar, itemSelectedDropDownName);
					DeleteFileA(aBorrar);
					refreshListView(currPath);
				}



			}
			break;
			}
		}
		break;
		case TVN_SELCHANGED:
		{

			DestroyWindow(list_hWnd);
			InitCommonControls();

			// icon type list view
			list_hWnd = CreateWindow(WC_LISTVIEW, LPCWSTR(""),
				WS_CHILD | WS_VISIBLE | LVS_ICON | LVS_ALIGNTOP | WS_VSCROLL,
				210, 40, 570, 502, hWnd, NULL, hInst, NULL); 
		//	ListView_DeleteAllItems(list_hWnd);


			// Assign image list to list view
			//ListView_SetImageList(list_hWnd, hImageList, LVSIL_NORMAL);

			strcpy(currPath, "C:\\");
			SetWindowTextA(path_hWnd, currPath);
			//OutputDebugStringA("INICIAL: ");
			//OutputDebugStringA(currPath);
			//OutputDebugStringA("\n");
			HTREEITEM hSelectedItem = TreeView_GetSelection(tree_hWnd);
			TVITEM tvi_temp = { 0 };
			tvi_temp.hItem = hSelectedItem;
			TCHAR nom[256];
			tvi_temp.cchTextMax = 256;
			tvi_temp.pszText = nom;

			tvi_temp.mask = TVIF_TEXT;
			TreeView_GetItem(tree_hWnd, &tvi_temp);

			LPWSTR w = tvi_temp.pszText;

			size_t size = wcstombs(NULL, (wchar_t*)tvi_temp.pszText, 0);
			char* charStr = new char[size + 1];
			wcstombs(charStr, (wchar_t*)tvi_temp.pszText, size + 1);


			GetWindowTextA(path_hWnd, currPath, 2048);
			//OutputDebugStringA(currPath);
			strcat(currPath, charStr);
			strcat(currPath, "\\");
			//OutputDebugStringA(currPath);
			//OutputDebugStringA("\n");
			SetWindowTextA(path_hWnd, currPath);

			AddItemToList(charStr);

		}
		break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void deleteDirNotEmpty(char* dir2) {
	WIN32_FIND_DATAA fdFile;
	static const std::string allFilesMask("*");

	char dir[2048];
	strcpy(dir, dir2);
	// First, delete the contents of the directory, recursively for subdirectories
	OutputDebugStringA("dir ant: ");
	OutputDebugStringA(dir);
	OutputDebugStringA("\n");
	strcat(dir, "\\*");
	//std::string searchMask = dir + allFilesMask;
	OutputDebugStringA("mask: ");
	OutputDebugStringA(dir);
	OutputDebugStringA("\n");
	HANDLE hFind = ::FindFirstFileA(dir, &fdFile);
	/*HANDLE hFind = ::FindFirstFileExA(
		searchMask.c_str(), FindExInfoBasic, &fdFile, FindExSearchNameMatch, nullptr, 0
	); */
	if (hFind == INVALID_HANDLE_VALUE) {
		DWORD lastError = ::GetLastError();
		if (lastError != ERROR_FILE_NOT_FOUND) { // or ERROR_NO_MORE_FILES, ERROR_NOT_FOUND?
			//throw std::runtime_error("Could not start directory enumeration");
			OutputDebugStringA("Could not start directory enumeration");
		}
	}

	// Did this directory have any contents? If so, delete them first
	if (hFind != INVALID_HANDLE_VALUE) {

		for (;;) {

			// Do not process the obligatory '.' and '..' directories
			if (strcmp(fdFile.cFileName, ".") != 0 && strcmp(fdFile.cFileName, "..") != 0) {
				char filePath[2048];
				char temp[2048];
				//strcpy(filePath, dir);
				strcpy(temp, dir);
				int a;
				for (a = strlen(temp); a > 0; a--) {
					if (temp[a] == '\\') {
						break;
					}
				}
				memset(filePath, 0, 2048);
				for (int b = 0; b <= a; b++) {
					filePath[b] = temp[b];
				}
				//strcat(filePath, "\\");
				strcat(filePath, fdFile.cFileName);
				OutputDebugStringA("fileName: ");
				OutputDebugStringA(filePath);
				OutputDebugStringA("\n");
				if (fdFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					OutputDebugStringA("si es dir");
					OutputDebugStringA("\n");
					if (PathIsDirectoryEmptyA(dir)) {
						char temp[2048];
						strcpy(temp, dir);
						int a;
						for (a = strlen(temp); a > 0; a--) {
							if (temp[a] == '\\') {
								break;
							}
						}
						memset(dir, 0, 2048);
						for (int b = 0; b <= a; b++) {
							dir[b] = temp[b];
						}
						BOOL result = ::RemoveDirectoryA(dir);
						if (result == FALSE) {
							OutputDebugStringA(" HERE error");
						}
					}
					else {
						OutputDebugStringA("not empty");
						OutputDebugStringA("\n");
						deleteDirNotEmpty(filePath);
					}
				}
				else {
					//if (fdFile.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN != 0) {
					OutputDebugStringA("es FIEL");
					OutputDebugStringA("\n");
					BOOL result = ::DeleteFileA(filePath);
					if (result == FALSE) {
						OutputDebugStringA("Could not delete file");
					}
					//}

				}
			}

			BOOL result = ::FindNextFileA(hFind, &fdFile);
			if (result == FALSE) {
				DWORD lastError = ::GetLastError();
				if (lastError == ERROR_NO_MORE_FILES) {
					if (!strcmp(dir, dir2)) {
						/*	char temp[2048];
							strcpy(temp, dir);
							int a;
							for (a = strlen(temp); a > 0; a--) {
								if (temp[a] == '\\') {
									break;
								}
							}
							memset(dir, 0, 2048);
							for (int b = 0; b <= a; b++) {
								dir[b] = temp[b];
							} */
						deleteDirNotEmpty(dir);
					}
					else {
						break;
					}
				}

			}

			// Advance to the next file in the directory


		} // for
	}
	OutputDebugStringA("dir ultimo: ");
	OutputDebugStringA(dir);
	OutputDebugStringA("\n");
	// The directory is empty, we can now safely remove it
	char temp[2048];
	strcpy(temp, dir);
	int a;
	for (a = strlen(temp); a > 0; a--) {
		if (temp[a] == '\\') {
			break;
		}
	}
	memset(dir, 0, 2048);
	for (int b = 0; b <= a; b++) {
		dir[b] = temp[b];
	}
	int ssi = strlen(dir);
	dir[ssi - 1] = ' ';
	OutputDebugStringA("dir ultimo cn t: ");
	OutputDebugStringA(dir);
	OutputDebugStringA("\n");
	BOOL result = ::RemoveDirectoryA(dir);
	if (result == FALSE) {
		OutputDebugStringA("ultimo error");
	}
}

bool isDir(char* name) {
	WIN32_FIND_DATAA fdFile;
	HANDLE hFind = 0;

	char nombre[2048];
	strcpy(nombre, currPath);
	strcat(nombre, name);
	OutputDebugStringA(nombre);
	hFind = FindFirstFileA(nombre, &fdFile);

	if (hFind == INVALID_HANDLE_VALUE) {
		OutputDebugStringA("dirrrrrr");
		return NULL;
	}

	if (fdFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
		return true;
	}
	else {
		return false;
	}
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
// Message handler for new file box.
INT_PTR CALLBACK NewFile(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:

		if (LOWORD(wParam) == IDOK) {
			char fileName[100];
			char contenido[4096];
			wchar_t contenido_w[4096];
			GetDlgItemTextA(hDlg, IDC_EDIT1, fileName, 100);
			UINT s = GetDlgItemText(hDlg, IDC_EDIT2, (LPWSTR)contenido_w, 4096);
			WideCharToMultiByte(CP_UTF8, 0, (LPCWCH)contenido_w, s, contenido, s, NULL, NULL);
			//char temp[2048];
			char fileNom[2048];
			strcpy(fileNom, currPath);

			/*int a;
			for (a = strlen(temp); a > 0; a--) {
				if (temp[a] == '\\') {
					//OutputDebugStringA("here");
					//OutputDebugStringA("\n");
					break;
				}
			}
			memset(fileNom, 0, 2048);
			for (int b = 0; b <= a; b++) {
				fileNom[b] = temp[b];
			} */
			OutputDebugStringA("fileName con path: ");
			OutputDebugStringA(fileNom);
			OutputDebugStringA("\n");
			strcat(fileNom, fileName);
			//OutputDebugStringA(fileName);
			/*OutputDebugStringA("fileName antes: ");
			OutputDebugStringA(fileNom);
			OutputDebugStringA("\n"); */
			createFile(hWnd, fileNom, contenido, s);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else if (LOWORD(wParam) == IDCANCEL) {
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK NewDir(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:

		if (LOWORD(wParam) == IDOK) {
			char dirName[100];
			GetDlgItemTextA(hDlg, IDC_EDIT1, dirName, 100);
			char dirNom[2048];
			strcpy(dirNom, currPath);
			strcat(dirNom, dirName);
			//OutputDebugStringA(dirName);
			//OutputDebugStringA("dir con name: ");
			//OutputDebugStringA(dirNom);
			//OutputDebugStringA("\n");
			createDir(hWnd, dirNom);
			refreshListView(currPath);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else if (LOWORD(wParam) == IDCANCEL) {
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

int createFile(HWND handler, char fileName[100], char contenido[4096], UINT size) {
	DWORD bytesWritten;
	OutputDebugStringA("Creando arhcivo con nombre: ");
	OutputDebugStringA(fileName);
	OutputDebugStringA("\n");
	HANDLE newFile = CreateFileA(fileName, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL | !FILE_ATTRIBUTE_DIRECTORY
		, NULL);

	if (newFile == INVALID_HANDLE_VALUE) {
		return -1;
	}

	bool res = WriteFile(newFile, contenido, size, &bytesWritten, NULL);

	CloseHandle(newFile);
	refreshListView(currPath);
	//DestroyWindow(tree_hWnd);
	//initTreeView();

	if (res)
		return 1;
	else
		return -1;
}

INT_PTR CALLBACK Move(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		SetDlgItemTextA(hDlg, IDC_EDIT1, currPath);
		return (INT_PTR)TRUE;

	case WM_COMMAND:

		if (LOWORD(wParam) == IDOK) {
			char pathAMoverlo[100];
			GetDlgItemTextA(hDlg, IDC_EDIT1, pathAMoverlo, 100);
			char currentFilePath[100];
			//char currentFilePath[100];
			//strcpy(dirNom, currPath);
			//strcat(dirNom, pathAMoverlo);
			//OutputDebugStringA(dirName);
			OutputDebugStringA(pathAMoverlo);

			//MoveFileA()
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		else if (LOWORD(wParam) == IDCANCEL) {
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

int createDir(HWND handler, char dirName[100]) {
	OutputDebugStringA("createDir: ");
	OutputDebugStringA(dirName);
	OutputDebugStringA("\n");
	bool newDir = CreateDirectoryA(dirName, NULL);

	if (!newDir) {
		return -1;
	}
	//TreeView_DeleteAllItems(tree_hWnd);
	//initTreeView();
	//DestroyWindow(tree_hWnd);
	//initTreeView();
	refreshListView(currPath);

	return 1;
}

INT_PTR CALLBACK VisualWindow(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:

		break;
	}
	return (INT_PTR)FALSE;
}

int getDirectoryContents(char* dir, HTREEITEM parent) {

	WIN32_FIND_DATAA fdFile;
	HANDLE hFind = 0;

	char path[2048];
	char newline[2];
	char dire[5];
	char file[5];
	//OutputDebugStringA(dir);
	sprintf(path, "%s*.*", dir); //queremos conseguir todo!!
	sprintf(newline, "%c", '\n');
	sprintf(dire, "%s", "Dir:");
	sprintf(file, "%s", "Fil:");
	hFind = FindFirstFileA(path, &fdFile);
	//OutputDebugStringA(path);
	//OutputDebugStringA(fdFile.cFileName);
	if (hFind == INVALID_HANDLE_VALUE) {
		//OutputDebugStringA((LPCSTR)hFind);
		//printf("Path not found: [%s]\n", dir);
		return -1;
	}

	TVITEMA tvi;
	TVINSERTSTRUCTA tvins;
	static HTREEITEM hPrev = (HTREEITEM)TVI_FIRST;
	static HTREEITEM hPrevRootItem = NULL;
	static HTREEITEM hPrevLev2Item = NULL;
	tvi.mask = TVIF_TEXT | TVIF_IMAGE
		| TVIF_SELECTEDIMAGE | TVIF_PARAM;


	do {
		if (strcmp((const char*)fdFile.cFileName, ".") != 0 && strcmp((const char*)fdFile.cFileName, "..") != 0 && strcmp((const char*)fdFile.cFileName, "$Recycle.Bin") != 0) { //quitar el retorno de . y .. por findfirstfile
			sprintf(path, "%s%s", dir, fdFile.cFileName);
			if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) { //si es directorio, en el nivel 2
				//OutputDebugStringA(fdFile.cFileName);
				//OutputDebugStringA("\n");
				if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0) {
					// Directories


				// Set the text of the item. 
					tvi.pszText = fdFile.cFileName;
					tvi.cchTextMax = sizeof(tvi.pszText) / sizeof(tvi.pszText[0]);
					HTREEITEM hPrev;
					// Assume the item is not a parent item, so give it a 
					// document image. 
					HIMAGELIST imageList = ImageList_Create(16, 16, ILC_COLORDDB | ILC_MASK, 1, 0);

					HICON icon;
					icon = (HICON)LoadImage(hInst, MAKEINTRESOURCE(IDI_FOLDER), IMAGE_ICON, 16
						, 16, LR_DEFAULTCOLOR);
					ImageList_AddIcon(imageList, icon);
					SendMessage(tree_hWnd, TVM_SETIMAGELIST, (WPARAM)TVSIL_NORMAL, (LPARAM)imageList);
					tvi.iImage = 0;
					//tvi.iSelectedImage = g_nDocument;

					// Save the heading level in the item's application-defined 
					// data area. 
					tvi.lParam = (LPARAM)parent;
					tvi.iSelectedImage = 1;
					tvins.item = tvi;


					//OutputDebugStringA(dire);
					//OutputDebugStringA(path);
					//OutputDebugStringA(newline);
					//FileNames.insert(sPath);
					tvins.hParent = parent;
					//sprintf(path, "%s%s", path, "\\");
					hPrev = (HTREEITEM)SendMessage(tree_hWnd, TVM_INSERTITEMA,
						0, (LPARAM)(LPTVINSERTSTRUCTA)& tvins);
					//sprintf(path, "\\%s",fdFile.cFileName);

					getDirectoryContents(path, hPrev); // Recursion

				}
			}
			else {
				if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0) {
					//Files 
					//AddItemToTree(hwndTree, sPath, 2);
				//	OutputDebugStringA(file);
					//OutputDebugStringA(path);
					//OutputDebugStringA(newline);
					tvins.hParent = parent;
					tvi.pszText = fdFile.cFileName;
					tvi.cchTextMax = sizeof(tvi.pszText) / sizeof(tvi.pszText[0]);
					hPrev = (HTREEITEM)SendMessage(tree_hWnd, TVM_INSERTITEMA,
						0, (LPARAM)(LPTVINSERTSTRUCTA)& tvins);
					//TreeView_SetItem(tree_hWnd, &tvi);
					tvins.hInsertAfter = hPrev;

					//fileNames.push_back((char*)path);
					//HTREEITEM p = AddItemToTree(tree_hWnd, (char*)path, parent, false);
					//getDirectoryContents(path,hPrev); // Recursion
				}
			}
		}
	} while (FindNextFileA(hFind, &fdFile));

	FindClose(hFind);

	return true;
}

HTREEITEM agregarNodoATree(char* name, int nivel, HTREEITEM parent, bool isDir) {
	OutputDebugStringA(name);
	TVINSERTSTRUCTA tvins;
	TVITEMA tvi;
	static HTREEITEM hPrev = (HTREEITEM)TVI_FIRST;
	static HTREEITEM hRootItem = NULL;
	static HTREEITEM hPrevLev2Item = NULL;
	tvi.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_PARAM;
	tvi.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIS_STATEIMAGEMASK;
	//tvi.iImage = AddIconToTree(hwndTree, text);
	//tvi.iSelectedImage = tvi.iImage;
	tvi.pszText = name;
	tvi.cchTextMax = sizeof(tvi.pszText) / sizeof(tvi.pszText[0]);
	tvins.hInsertAfter = hPrev;
	tvins.item = tvi;

	if (nivel == 1) { //root
		tvins.hParent = TVI_ROOT;
	}
	else if (nivel == 2) {

		tvins.hParent = hRootItem;
	}
	else
	{
		TVITEM tviSetup;
		tviSetup.hItem = hPrev;
		tviSetup.mask = TVIF_PARAM;
		TVITEM* tviLocal = &tviSetup;
		TreeView_GetItem(tree_hWnd, tviLocal);

		if (nivel > tviLocal->lParam) {
			tvins.hParent = hPrev;
		}
		else {
			HTREEITEM hPrevLocal = TreeView_GetParent(tree_hWnd, hPrev);
			tviLocal->hItem = hPrevLocal;
			TreeView_GetItem(tree_hWnd, tviLocal);
			for (int i = nivel; i <= tviLocal->lParam;) {
				HTREEITEM hPrevLocalTemp = TreeView_GetParent(tree_hWnd, hPrevLocal);
				hPrevLocal = hPrevLocalTemp;
				tviLocal->hItem = hPrevLocal;
				TreeView_GetItem(tree_hWnd, tviLocal);
			}
			tviLocal->mask = TVIF_TEXT;
			TreeView_GetItem(tree_hWnd, tviLocal);
			tvins.hParent = hPrevLocal;

		}
	}

	hPrev = (HTREEITEM)SendMessage(tree_hWnd, TVM_INSERTITEMA, 0, (LPARAM)(LPTVINSERTSTRUCTA)& tvins);

	if (hPrev == 0) {
		return false;
	}
	if (nivel == 1) {
		hRootItem = hPrev;
	}

	return hPrev;
}

void AddItemToList(char* item) {
	WIN32_FIND_DATAA fdFile;
	HANDLE hFind = 0;


	LV_ITEM lvI;
	ZeroMemory(&lvI, sizeof(LV_ITEM));

	char path[2048] = "C:\\";
	char newline[2];
	sprintf(newline, "%c", '\n');
	strcat(path, item);
	strcat(path, "\\");
	sprintf(path, "%s*", path);

	if (hFind == INVALID_HANDLE_VALUE) {
		//OutputDebugStringA("nops");
	}

	//strcat(path, fdFile.cFileName);
	hFind = FindFirstFileA(path, &fdFile);

	HIMAGELIST imageList = ImageList_Create(32, 32, ILC_COLORDDB | ILC_MASK, 1, 0);

	HICON icon, icon2;
	icon = (HICON)LoadImage(hInst, MAKEINTRESOURCE(IDI_FOLDER), IMAGE_ICON, 32
		, 32, LR_DEFAULTCOLOR);
	ImageList_AddIcon(imageList, icon);

	icon2 = (HICON)LoadImage(hInst, MAKEINTRESOURCE(IDI_FILE), IMAGE_ICON, 32
		, 32, LR_DEFAULTCOLOR);
	ImageList_AddIcon(imageList, icon2);

	LRESULT m = SendMessage(list_hWnd, LVM_SETIMAGELIST, (WPARAM)TVSIL_NORMAL, (LPARAM)imageList);
	//OutputDebugStringA(path);
	//OutputDebugStringA(newline);
	//OutputDebugStringA(fdFile.cFileName);

	do {
		if (strcmp((const char*)fdFile.cFileName, ".") != 0 && strcmp((const char*)fdFile.cFileName, "..") != 0) { //quitar el retorno de . y .. por findfirstfile
			//strcat(path, fdFile.cFileName);																					   //sprintf(path, "%s", fdFile.cFileName);
			if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) { //si es directorio, en el nivel 2
				//OutputDebugStringA(fdFile.cFileName);
				//OutputDebugStringA("\n");
				if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0) { //los not hidden
					// Directories

					lvI.mask = LVIF_TEXT | LVIF_IMAGE;
					lvI.iImage = 0;
					lvI.pszText = (LPWSTR)fdFile.cFileName;


					lvI.stateMask = 0;
					lvI.iSubItem = 0;
					lvI.state = 0;
					//MessageBoxA(list_hWnd,lvI.pszText, "Text", MB_OK);
					SendMessage(list_hWnd, LVM_INSERTITEMA,
						0, (LPARAM)& lvI);

				}

			}
			else { //archivos, diff image
				//OutputDebugStringA(fdFile.cFileName);
				//OutputDebugStringA("\n");
				if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0) { //los not hidden

					lvI.mask = LVIF_TEXT | LVIF_IMAGE;
					lvI.iImage = 1;
					lvI.pszText = (LPWSTR)fdFile.cFileName;


					lvI.stateMask = 0;
					lvI.iSubItem = 0;
					lvI.state = 0;
					//MessageBoxA(list_hWnd,lvI.pszText, "Text", MB_OK);
					SendMessage(list_hWnd, LVM_INSERTITEMA,
						0, (LPARAM)& lvI);

				}
			}

		}
	} while (FindNextFileA(hFind, &fdFile));

	FindClose(hFind);

}

void refreshListView(char* path2) {
	WIN32_FIND_DATAA fdFile;
	HANDLE hFind = 0;
	OutputDebugStringA(path2);
	OutputDebugStringA("\n");

	ListView_DeleteAllItems(list_hWnd);

	LV_ITEM lvI;
	ZeroMemory(&lvI, sizeof(LV_ITEM));
	char path[2048];
	strcpy(path, path2);

	sprintf(path, "%s*", path);

	//strcat(path, fdFile.cFileName);
	hFind = FindFirstFileA(path, &fdFile);
	if (hFind == INVALID_HANDLE_VALUE) {
		OutputDebugStringA("nops");
		wchar_t err[256];
		memset(err, 0, 256);
		FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
		int msgboxID = MessageBoxW(NULL,
			err,
			(LPCWSTR)L"ERROR",
			MB_OK);
	}

	HIMAGELIST imageList = ImageList_Create(32, 32, ILC_COLORDDB | ILC_MASK, 1, 0);

	HICON icon, icon2;
	icon = (HICON)LoadImage(hInst, MAKEINTRESOURCE(IDI_FOLDER), IMAGE_ICON, 32
		, 32, LR_DEFAULTCOLOR);
	ImageList_AddIcon(imageList, icon);

	icon2 = (HICON)LoadImage(hInst, MAKEINTRESOURCE(IDI_FILE), IMAGE_ICON, 32
		, 32, LR_DEFAULTCOLOR);
	ImageList_AddIcon(imageList, icon2);

	LRESULT m = SendMessage(list_hWnd, LVM_SETIMAGELIST, (WPARAM)TVSIL_NORMAL, (LPARAM)imageList);
	//OutputDebugStringA(path);

	OutputDebugStringA(fdFile.cFileName);
	OutputDebugStringA("\n");

	do {
		if (strcmp((const char*)fdFile.cFileName, ".") != 0 && strcmp((const char*)fdFile.cFileName, "..") != 0) { //quitar el retorno de . y .. por findfirstfile
			//strcat(path, fdFile.cFileName);																					   //sprintf(path, "%s", fdFile.cFileName);
			if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) { //si es directorio, en el nivel 2
				//OutputDebugStringA(fdFile.cFileName);
				//OutputDebugStringA("\n");
				if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0) { //los not hidden
					// Directories

					lvI.mask = LVIF_TEXT | LVIF_IMAGE;
					lvI.iImage = 0;
					lvI.pszText = (LPWSTR)fdFile.cFileName;


					lvI.stateMask = 0;
					lvI.iSubItem = 0;
					lvI.state = 0;
					//MessageBoxA(list_hWnd,(LPCSTR)lvI.pszText, "Text", MB_OK);
					SendMessage(list_hWnd, LVM_INSERTITEMA,
						0, (LPARAM)& lvI);

				}

			}
			else { //archivos, diff image
				//OutputDebugStringA(fdFile.cFileName);
				//OutputDebugStringA("\n");
				if ((fdFile.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0) { //los not hidden
				//	OutputDebugStringA("here");
				//	OutputDebugStringA(fdFile.cFileName);
					lvI.mask = LVIF_TEXT | LVIF_IMAGE;
					lvI.iImage = 1;
					lvI.pszText = (LPWSTR)fdFile.cFileName;

					lvI.stateMask = 0;
					lvI.iSubItem = 0;
					lvI.state = 0;
					//MessageBoxA(list_hWnd,lvI.pszText, "Text", MB_OK);
					SendMessage(list_hWnd, LVM_INSERTITEMA,
						0, (LPARAM)& lvI);

				}
			}

		}
	} while (FindNextFileA(hFind, &fdFile));

	FindClose(hFind);
}
