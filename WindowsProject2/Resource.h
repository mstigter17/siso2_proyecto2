//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Proyecto2.rc
//
#define IDC_MYICON                      2
#define IDM_TREEVIEW                    4
#define IDM_LISTVIEW                    5
#define IDD_MAINWINDOW                  101
#define IDD_PROYECTO2_DIALOG            102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_PROYECTO2                   107
#define IDI_SMALL                       108
#define IDC_PROYECTO2                   109
#define IDR_MAINFRAME                   128
#define IDD_NEWFILE                     129
#define IDI_FILE                        132
#define IDB_PNG1                        134
#define IDR_MENU1                       137
#define IDI_FOLDER                      138
#define IDI_BACK                        140
#define IDI_ICON2                       141
#define IDI_FORWARD                     141
#define IDD_DIALOG1                     142
#define IDD_NEWDIR                      142
#define IDR_MENU2                       144
#define IDD_DIALOG2                     145
#define IDD_MOVE                        145
#define IDC_EDIT1                       1000
#define IDC_EDIT2                       1001
#define ID_FILE_NEW                     32771
#define ID_NEW_ARCHIVO                  32772
#define ID_NEW_CARPETA                  32773
#define ID_COPIAR_MOVER                 32776
#define ID_COPIAR_BORRAR                32777
#define ID_COPIAR_BORRAR32778           32778
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        146
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
